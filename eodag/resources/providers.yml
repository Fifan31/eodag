# -*- coding: utf-8 -*-
# Copyright 2018, CS Systemes d'Information, http://www.c-s.fr
#
# This file is part of EODAG project
#     https://www.github.com/CS-SI/EODAG
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

USGS:
  priority: 0
  api:
    plugin: UsgsApi
    google_base_url: 'http://storage.googleapis.com/earthengine-public/landsat/'
    metadata_mapping:
      id: '$.entityId'
      geometry: '$.geometry'
      productType: '$.productType'
      title: '$.displayId'
      abstract: '$.summary'
      startTimeFromAscendingNode: '$.startTime'
      completionTimeFromAscendingNode: '$.endTime'
      quicklook: '$.browserUrl'
  products:
    L8_OLI_TIRS_C1L1:
      dataset: LANDSAT_8_C1
      catalog_node: EE  # Possible values: CWIC, EE, HDDS, LPCS

AmazonWS:
  priority: 0
  search:
    plugin: AwsSearch
    api_endpoint: 'http://finder.eocloud.eu/resto/api/'
    product_location_scheme: 's3'
    metadata_mapping:
      id: '$.id'
      geometry:
        - box
        - '$.geometry'
      productType:
        - productType
        - '$.properties.productType'
      platform: '$.properties.collection'
      platformSerialIdentifier: '$.properties.platform'
      instrument: '$.properties.instrument'
      processingLevel: '$.properties.processingLevel'
      title: '$.properties.title'
      keyword: '$.properties.keywords'
      abstract: '$.properties.description'
      organisationName: '$.properties.organisationName'
      orbitNumber: '$.properties.orbitNumber'
      orbitDirection: '$.properties.orbitDirection'
      cloudCover:
        - cloudCover
        - '$.properties.cloudCover'
      snowCover: '$.properties.snowCover'
      startTimeFromAscendingNode:
        - startDate
        - '$.properties.startDate'
      completionTimeFromAscendingNode:
        - completionDate
        - '$.properties.completionDate'
      parentIdentifier: '$.properties.parentIdentifier'
      resolution: '$.properties.resolution'
      sensorMode: '$.properties.sensorMode'
      quicklook: '$.properties.quicklook'
  products:
    S2_MSI_L1C:
      product_type: L1C
      collection: Sentinel2
  download:
    plugin: AwsDownload
    associated_bucket: 'sentinel-s2-l1c'
  auth:
    plugin: OAuth

theia-landsat:
  priority: 0
  products:
    LS_REFLECTANCE:
      collection: Landsat
      product_type: REFLECTANCE
    LS_REFLECTANCETOA:
      collection: Landsat
      product_type: REFLECTANCETOA
    PLD_REFLECTANCE:
      collection: Pleiades
      product_type: REFLECTANCE
    PLD_REFLECTANCETOA:
      collection: Pleiades
      product_type: REFLECTANCETOA
    PLD_BUNDLE:
      collection: Pleiades
      product_type: Bundle (Pan, XS)
  search:
    plugin: RestoSearch
    api_endpoint: 'https://theia-landsat.cnes.fr/resto/api/'
    metadata_mapping:
      id: '$.id'
      geometry:
        - box
        - '$.geometry'
      productType:
        - productType
        - '$.properties.productType'
      platform: '$.properties.collection'
      platformSerialIdentifier: '$.properties.platform'
      instrument: '$.properties.instrument'
      processingLevel: '$.properties.processingLevel'
      title: '$.properties.title'
      keyword: '$.properties.keywords'
      organisationName: '$.properties.organisationName'
      orbitNumber: '$.properties.orbitNumber'
      orbitDirection: '$.properties.orbitDirection'
      cloudCover:
        - cloudCover
        - '$.properties.cloudCover'
      snowCover: '$.properties.snowCover'
      startTimeFromAscendingNode:
        - startDate
        - '$.properties.startDate'
      completionTimeFromAscendingNode:
        - completionDate
        - '$.properties.completionDate'
      parentIdentifier: '$.properties.parentIdentifier'
      resolution: '$.properties.resolution'
      sensorMode: '$.properties.sensorMode'
      productVersion: '$.properties.version'
      quicklook: '$.properties.quicklook'
  download:
    plugin: HTTPDownload
    base_uri: 'https://theia-landsat.cnes.fr/resto'
  auth:
    plugin: OIDCAuthorizationCodeFlowAuth
    authorization_uri: 'https://sso.theia-land.fr/oauth2/authorize'
    redirect_uri: 'https://theia-landsat.cnes.fr/rocket/'
    authentication_uri: 'https://sso.theia-land.fr/commonauth'
    token_uri: 'https://theia-landsat.cnes.fr/resto/api/auth/theia'
    client_id: 'ZYhcLtyuDM0f2BMH1bIF1Fc2neQa'
    token_exchange_params:
      redirect_uri: redirectUri
      client_id: clientId
    token_exchange_post_data_method: json   # One of json, data, params
    token_provision: qs # One of qs or header. If qs, token_qs_key is mandatory
    token_qs_key: '_bearer'

theia:
  priority: 0
  search:
    plugin: RestoSearch
    api_endpoint: 'https://theia.cnes.fr/atdistrib/resto2/api'
    metadata_mapping:
      id: '$.id'
      geometry:
        - box
        - '$.geometry'
      productType:
        - productType
        - '$.properties.productType'
      platformSerialIdentifier: '$.properties.platform'
      instrument: '$.properties.instrument'
      processingLevel: '$.properties.processingLevel'
      title: '$.properties.title'
      keyword: '$.properties.keywords'
      organisationName: '$.properties.organisationName'
      orbitNumber: '$.properties.orbitNumber'
      orbitDirection: '$.properties.orbitDirection'
      cloudCover:
        - cloudCover
        - '$.properties.cloudCover'
      snowCover: '$.properties.snowCover'
      startTimeFromAscendingNode:
        - startDate
        - '$.properties.startDate'
      completionTimeFromAscendingNode:
        - completionDate
        - '$.properties.completionDate'
      parentIdentifier: '$.properties.parentIdentifier'
      resolution: '$.properties.resolution'
      sensorMode: '$.properties.sensorMode'
      productVersion: '$.properties.version'
      quicklook: '$.properties.quicklook'
  products:
    S2_MSI_L2A:
      product_type: REFLECTANCE
      collection: SENTINEL2
  download:
    plugin: HTTPDownload
    base_uri: 'https://theia.cnes.fr/atdistrib/resto2'
    dl_url_params:
      issuerId: theia
  auth:
    plugin: TokenAuth
    auth_uri: 'https://theia.cnes.fr/atdistrib/services/authenticate/'

peps:
  priority: 1
  search:
    plugin: RestoSearch
    api_endpoint: 'https://peps.cnes.fr/resto/api'
    metadata_mapping:
      id: '$.id'
      geometry:
        - box
        - '$.geometry'
      productType:
        - productType
        - '$.properties.productType'
      platform: '$.properties.collection'
      platformSerialIdentifier: '$.properties.platform'
      instrument: '$.properties.instrument'
      processingLevel: '$.properties.processingLevel'
      title: '$.properties.title'
      keyword: '$.properties.keywords'
      abstract: '$.properties.description'
      organisationName: '$.properties.organisationName'
      orbitNumber: '$.properties.orbitNumber'
      orbitDirection: '$.properties.orbitDirection'
      cloudCover:
        - cloudCover
        - '$.properties.cloudCover'
      snowCover: '$.properties.snowCover'
      startTimeFromAscendingNode:
        - startDate
        - '$.properties.startDate'
      completionTimeFromAscendingNode:
        - completionDate
        - '$.properties.completionDate'
      parentIdentifier: '$.properties.parentIdentifier'
      resolution: '$.properties.resolution'
      sensorMode: '$.properties.sensorMode'
      quicklook: '$.properties.quicklook'
  products:
    S1_SAR_OCN:
      product_type: OCN
      collection: S1
    S1_SAR_GRD:
      product_type: GRD
      collection: S1
    S1_SAR_SLC:
      product_type: SLC
      collection: S1
    S2_MSI_L1C:
      collection: S2ST
      product_type: S2MSI1C
      partial: false   # Peps only provide L1C products for Sentinel-2A platform
    S3_EFR:
      product_type: OL_1_EFR___
      collection: S3
    S3_ERR:
      product_type: OL_1_ERR___
      collection: S3
    S3_OLCI_L2LFR:
      product_type: OL_2_LFR___
      collection: S3
    S3_OLCI_L2LRR:
      product_type: OL_2_LRR___
      collection: S3
    S3_SLSTR_L1RBT:
      product_type: SL_1_RBT___
      collection: S3
    S3_SLSTR_L2LST:
      product_type: SL_2_LST___
      collection: S3
    S3_LAN:
      product_type: SR_2_LAN___
      collection: S3
  download:
    plugin: HTTPDownload
    base_uri: 'https://peps.cnes.fr/resto'
    archive_depth: 2
    dl_url_params:
      issuerId: peps
  auth:
    plugin: GenericAuth
    auth_uri: 'https://peps.cnes.fr/resto/api/users/connect'

airbus-ds:
  priority: 0
  products:
    S2_MSI_L1C:
      product_type: S2MSI1C
      partial: true
    S1_SAR_RAW:
      product_type: RAW
      partial: true
    S1_SAR_GRD:
      product_type: GRD
      partial: true
    S1_SAR_SLC:
      product_type: SLC
      partial: true
    S1_SAR_OCN:
      product_type: OCN
      partial: true
    S3_LAN:
      product_type: SR_2_LAN___
    S3_SRA:
      product_type: SR_1_SRA___
    S3_SRA_BS:
      product_type: SR_1_SRA_BS
    S3_SRA_A_BS:
      product_type: SR_1_SRA_A_
  search:
    plugin: ArlasSearch
    api_endpoint: 'https://sobloo.eu/api/v1/services/explore'
    quicklook_endpoint: 'https://sobloo.eu/api/v1/services/quicklook'
    arlas_collection: catalog
    metadata_mapping:
      id: '$.properties.uid'
      geometry:
        - gintersect
        - '$.geometry'
      productType:
        - 'identification.type'
        - '$.properties.identification.type'
      platform: '$.properties.acquisition.mission'
      platformSerialIdentifier: '$.properties.acquisition.missionCode'
      instrument: '$.properties.acquisition.sensorId'
      processingLevel: '$.properties.production.levelCode'
      title: '$.properties.identification.externalId'
      orbitNumber: '$.properties.orbit.number'
      orbitDirection: '$.properties.orbit.direction'
      cloudCover:
        - 'contentDesctiption.cloudCoverPercentage'
        - '$.properties.contentDescription.cloudCoverPercentage'
      startTimeFromAscendingNode:
        - 'acquisition.beginViewingDate'
        - '$.properties.acquisition.beginViewingDate'
      completionTimeFromAscendingNode: '$.properties.acquisition.endViewingDate'
      quicklook: '$.properties.state.resources.quicklook'
  download:
    plugin: HTTPDownload
    base_uri: 'https://sobloo.eu/api/v1/services/download/'
    archive_depth: 2
  auth:
    plugin: HTTPHeaderAuth
    headers:
      Authorization: "Apikey {apikey}"
