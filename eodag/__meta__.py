# -*- coding: utf-8 -*-
# Copyright 2018, CS Systemes d'Information, http://www.c-s.fr
#
# This file is part of EODAG project
#     https://www.github.com/CS-SI/EODAG
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from __future__ import unicode_literals


__title__ = 'eodag'
__description__ = 'Earth Observation Data Access Gateway'
__version__ = '0.6.3'
__author__ = "CS Systemes d'Information (CSSI)"
__author_email__ = 'admin@geostorm.eu'
__url__ = 'https://bitbucket.org/geostorm/eodag'
__license__ = 'Apache 2.0'
__copyright__ = "Copyright 2018, CS Systemes d'Information, http://www.c-s.fr"
