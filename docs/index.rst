.. eodag documentation master file, created by
   sphinx-quickstart on Thu Feb  1 09:22:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to eodag's documentation!
=================================

The brand new Earth Observation Data Access Gateway:
https://bitbucket.org/geostorm/eodag

-------------------------------------------------------------------------------

.. toctree::
   :maxdepth: 3

   intro
   tutos
   install
   use
   api
   contribute
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
